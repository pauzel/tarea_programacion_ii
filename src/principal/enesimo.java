package principal;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class enesimo {
   
    	JFrame en = new JFrame("ENESIMO FACTORIAL");
    	
    	JLabel titulo = new JLabel();
    	JLabel setnum_1 = new JLabel();
    	JLabel setres = new JLabel();
    	
    	JTextField getnum_1 = new JTextField();
    	JTextField getres = new JTextField();
    	
    	JButton resultado = new JButton();
    	JButton volver = new JButton();
    	JButton limpiar = new JButton();
    	
    	void enes() {
    		en.setLayout(null);
    		
    		getres.setEditable(false);
    		
    		titulo.setText("Bienvenido Al Enesimo");
    		titulo.setBounds(60,20,180,15);
    		titulo.setForeground(Color.RED);
    		
    		setnum_1.setText("Numero: ");
    		setnum_1.setBounds(25,60,60,15);
    		setnum_1.setForeground(Color.BLUE);
    		
    		getnum_1.setBounds(90,50,100,30);
    		
    		setres.setText("Enesimo:");
    		setres.setBounds(25,110,60,15);
    		setres.setForeground(Color.BLUE);
    		
    		getres.setBounds(90,100,150,30);
    		
    		limpiar.setText("Limpiar");
    		limpiar.setBounds(10,160,80,30);
    		limpiar.setBackground(Color.cyan);
    		limpiar.setForeground(Color.BLACK);
    		
    		resultado.setText("Enesimo");
    		resultado.setBounds(100,160,80,30);
    		resultado.setBackground(Color.cyan);
    		resultado.setForeground(Color.BLACK);
    		
    		volver.setText("Atras");
    		volver.setBounds(190,160,80,30);
    		volver.setBackground(Color.cyan);
    		volver.setForeground(Color.BLACK);
    		
    		en.add(titulo);
    		en.add(setnum_1);
    		en.add(getnum_1);
    		en.add(setres);
    		en.add(getres);	
    		en.add(resultado);
    		en.add(volver);
    		en.add(limpiar);
    		
    		limpiar.addActionListener(new ActionListener() {
        		public void actionPerformed(ActionEvent evt) {
        			getnum_1.setText("");
        			getres.setText("");
        		}
        	});
    		
    		resultado.addActionListener(new ActionListener() {
    			public void actionPerformed(ActionEvent evt) {
    				int numero,fibo1,fibo2,i;

   		            numero = Integer.parseInt(getnum_1.getText());
    		        
    		        
    		        fibo1=1;
    		        fibo2=1;

    		        for(i=2;i<=numero;i++){
    		             
    		             fibo2 = fibo1 + fibo2;
    		             fibo1 = fibo2 - fibo1;
    		        }
    		        getres.setText(""+fibo1);
    			}
    		});
    		
    		volver.addActionListener(new ActionListener() {
    			public void actionPerformed(ActionEvent evt) {
    				interfaz.principal();
    				en.dispose();
    			}
    		});
    		
    		en.setLocationRelativeTo(null);
    		en.setSize(300,250);
    		en.setVisible(true);
    	}
}