package principal;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class factorial {
   
    	JFrame fc = new JFrame("FACTORIAL");
    	
    	JLabel titulo = new JLabel();
    	JLabel setnum_1 = new JLabel();
    	JLabel setres = new JLabel();
    	
    	JTextField getnum_1 = new JTextField();
    	JTextField getres = new JTextField();
    	
    	JButton resultado = new JButton();
    	JButton volver = new JButton();
    	JButton limpiar = new JButton();
    	
    	void fact() {
    		fc.setLayout(null);
    		
    		getres.setEditable(false);
    		
    		titulo.setText("Bienvenido Al Numero Factorial");
    		titulo.setBounds(60,20,180,15);
    		titulo.setForeground(Color.RED);
    		
    		setnum_1.setText("Numero: ");
    		setnum_1.setBounds(25,60,60,15);
    		setnum_1.setForeground(Color.BLUE);
    		
    		getnum_1.setBounds(90,50,100,30);
    		
    		setres.setText("Factorial:");
    		setres.setBounds(25,110,60,15);
    		setres.setForeground(Color.BLUE);
    		
    		getres.setBounds(90,100,150,30);
    		
    		limpiar.setText("Limpiar");
    		limpiar.setBounds(10,160,80,30);
    		limpiar.setBackground(Color.cyan);
    		limpiar.setForeground(Color.BLACK);
    		
    		resultado.setText("Factorial");
    		resultado.setBounds(100,160,80,30);
    		resultado.setBackground(Color.cyan);
    		resultado.setForeground(Color.BLACK);
    		
    		volver.setText("Atras");
    		volver.setBounds(190,160,80,30);
    		volver.setBackground(Color.cyan);
    		volver.setForeground(Color.BLACK);
    		
    		fc.add(titulo);
    		fc.add(setnum_1);
    		fc.add(getnum_1);
    		fc.add(setres);
    		fc.add(getres);	
    		fc.add(resultado);
    		fc.add(volver);
    		fc.add(limpiar);
    		
    		limpiar.addActionListener(new ActionListener() {
        		public void actionPerformed(ActionEvent evt) {
        			getnum_1.setText("");
        			getres.setText("");
        		}
        	});
    		
    		resultado.addActionListener(new ActionListener() {
    			public void actionPerformed(ActionEvent evt) {
    		        int num,i;
    		        long factorial = 1;
    		        
    		        num = Integer.parseInt(getnum_1.getText());
    		        
    		        for(i=num;i>0;i--){
    		            factorial = factorial * i;
    		        }
    		        
    		        getres.setText(""+factorial);
    			}
    		});
    		
    		volver.addActionListener(new ActionListener() {
    			public void actionPerformed(ActionEvent evt) {
    				interfaz.principal();
    				fc.dispose();
    			}
    		});
    		
    		fc.setLocationRelativeTo(null);
    		fc.setSize(300,250);
    		fc.setVisible(true);
    	}
}
