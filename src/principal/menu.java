package principal;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class menu {
	JFrame men = new JFrame("Menu Calculadora");
	JLabel label1 = new JLabel();
	JLabel label2 = new JLabel();
	JButton suma = new JButton();
	JButton resta = new JButton();
	JButton division = new JButton();
	JButton multiplicacion = new JButton();
	JButton primo = new JButton();
	JButton factorial = new JButton();
	JButton enefibo = new JButton();
	JButton seriefibo = new JButton();
	JLabel label3 = new JLabel();
	
	void Menu() {
		men.setLayout(null);
		
		label1.setText("Bienvenido A La Calculadora Basica");
		label1.setBounds(90,10,220,20);
		label1.setForeground(Color.red);
		
		label2.setText("Seleccione una opcion: ");
		label2.setBounds(120,50,200,20);
		label2.setForeground(Color.green);
		
		label3.setText("By Paulino Zelaya");
		label3.setBounds(150,240,150,15);
		label3.setForeground(Color.blue);
		
		suma.setText("SUMA");
		suma.setBounds(30,80,150,30);
		suma.setBackground(Color.cyan);
		suma.setForeground(Color.BLACK);
		
		resta.setText("RESTA");
		resta.setBounds(210,80,150,30);
		resta.setBackground(Color.cyan);
		resta.setForeground(Color.BLACK);
		
		division.setText("DIVISION");
		division.setBounds(30,120,150,30);
		division.setBackground(Color.cyan);
		division.setForeground(Color.BLACK);
		
		multiplicacion.setText("MULTIPLICACION");
		multiplicacion.setBounds(210,120,150,30);
		multiplicacion.setBackground(Color.cyan);
		multiplicacion.setForeground(Color.BLACK);
		
		primo.setText("NUMERO PRIMO");
		primo.setBounds(30,160,150,30);
		primo.setBackground(Color.cyan);
		primo.setForeground(Color.BLACK);
		
		factorial.setText("FACTORIAL");
		factorial.setBounds(210,160,150,30);
		factorial.setBackground(Color.cyan);
		factorial.setForeground(Color.BLACK);
		
		enefibo.setText("ENESIMO FIBONACCI");
		enefibo.setBounds(30,200,150,30);
		enefibo.setBackground(Color.cyan);
		enefibo.setForeground(Color.BLACK);
		
		seriefibo.setText("SERIE FIBONACCI");
		seriefibo.setBounds(210,200,150,30);
		seriefibo.setBackground(Color.cyan);
		seriefibo.setForeground(Color.BLACK);
		
		men.add(label1);
		men.add(label2);
		men.add(suma);
		men.add(resta);
		men.add(division);
		men.add(multiplicacion);
		men.add(primo);
		men.add(factorial);
		men.add(enefibo);
		men.add(seriefibo);
		men.add(label3);
		
		suma.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent evt) {
    			suma sum = new suma();
    			sum.sumando();
    			men.dispose();
    		}
    	});
		
		resta.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent evt) {
    			resta res = new resta();
    			res.restando();
    			men.dispose();
    		}
    	});
		
		division.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent evt) {
    			division div = new division();
    			div.dividiendo();
    			men.dispose();
    		}
    	});
		
		multiplicacion.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent evt) {
    			multiplicar mul = new multiplicar();
    			mul.multiplicando();
    			men.dispose();
    		}
    	});
		
		division.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent evt) {
    			suma sum = new suma();
    			sum.sumando();
    			men.dispose();
    		}
    	});
		
		primo.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent evt) {
    			primo pr = new primo();
    			pr.PRIMO();
    			men.dispose();
    		}
    	});
		
		factorial.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent evt) {
    			factorial fac = new factorial();
    			fac.fact();
    			men.dispose();
    		}
    	});
		
		enefibo.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent evt) {
    			enesimo en = new enesimo();
    			en.enes();
    			men.dispose();
    		}
    	});
		
		seriefibo.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent evt) {
    			serie_fibonacci se = new serie_fibonacci();
    			se.serie();
    			men.dispose();
    		}
    	});
		
		men.setLocationRelativeTo(null);
		men.setSize(400,300); 
		men.setVisible(true);		
	}
}