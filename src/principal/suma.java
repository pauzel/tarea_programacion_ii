package principal;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class suma {
	JFrame sum = new JFrame("SUMA");
	
	JLabel titulo = new JLabel();
	JLabel setnum_1 = new JLabel();
	JLabel setnum_2 = new JLabel();
	JLabel setres = new JLabel();
	
	JTextField getnum_1 = new JTextField();
	JTextField getnum_2 = new JTextField();
	JTextField getres = new JTextField();
	
	JButton resultado = new JButton();
	JButton volver = new JButton();
	JButton limpiar = new JButton();
	
	void sumando() {
		sum.setLayout(null);
		
		getres.setEditable(false);
		
		titulo.setText("Bienvenido A La Suma");
		titulo.setBounds(85,20,150,15);
		titulo.setForeground(Color.RED);
		
		setnum_1.setText("Numero 1: ");
		setnum_1.setBounds(25,60,70,15);
		setnum_1.setForeground(Color.BLUE);
		
		setnum_2.setText("Numero 2: ");
		setnum_2.setBounds(155,60,70,15);
		setnum_2.setForeground(Color.BLUE);
		
		getnum_1.setBounds(95,50,50,30);
		
		getnum_2.setBounds(225,50,50,30);
		
		setres.setText("Resultado");
		setres.setBounds(85,110,60,15);
		setres.setForeground(Color.BLUE);
		
		getres.setBounds(155,100,50,30);
		
		limpiar.setText("Limpiar");
		limpiar.setBounds(10,160,80,30);
		limpiar.setBackground(Color.cyan);
		limpiar.setForeground(Color.BLACK);
		
		resultado.setText("Sumar");
		resultado.setBounds(100,160,80,30);
		resultado.setBackground(Color.cyan);
		resultado.setForeground(Color.BLACK);
		
		volver.setText("Atras");
		volver.setBounds(190,160,80,30);
		volver.setBackground(Color.cyan);
		volver.setForeground(Color.BLACK);
		
		sum.add(titulo);
		sum.add(setnum_1);
		sum.add(getnum_1);
		sum.add(setnum_2);
		sum.add(getnum_2);
		sum.add(setres);
		sum.add(getres);	
		sum.add(resultado);
		sum.add(volver);
		sum.add(limpiar);
		
		limpiar.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent evt) {
    			getnum_1.setText("");
    			getnum_2.setText("");
    			getres.setText("");
    		}
    	});
		
		resultado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				int num1,num2,res;
				
				num1 = Integer.parseInt(getnum_1.getText());
				num2 = Integer.parseInt(getnum_2.getText());
				res = num1 + num2;
				getres.setText(String.format("%d",res));
			}
		});
		
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				interfaz.principal();
				sum.dispose();
			}
		});
		
		sum.setLocationRelativeTo(null);
		sum.setSize(300,250);
		sum.setVisible(true);
	}
}