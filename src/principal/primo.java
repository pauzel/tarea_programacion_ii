package principal;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class primo {
	JFrame pr = new JFrame("NUMERO PRIMO");
	
	JLabel titulo = new JLabel();
	JLabel setnum_1 = new JLabel();
	JLabel setres = new JLabel();
	
	JTextField getnum_1 = new JTextField();
	JTextField getres = new JTextField();
	
	JButton resultado = new JButton();
	JButton volver = new JButton();
	JButton limpiar = new JButton();
	
	void PRIMO() {
		pr.setLayout(null);
		
		getres.setEditable(false);
		
		titulo.setText("Bienvenido A Los Primos");
		titulo.setBounds(85,20,150,15);
		titulo.setForeground(Color.RED);
		
		setnum_1.setText("Numero A Probar: ");
		setnum_1.setBounds(25,60,105,15);
		setnum_1.setForeground(Color.BLUE);
		
		getnum_1.setBounds(135,50,100,30);
		
		setres.setText("Resultado");
		setres.setBounds(25,110,60,15);
		setres.setForeground(Color.BLUE);
		
		getres.setBounds(90,100,150,30);
		
		limpiar.setText("Limpiar");
		limpiar.setBounds(10,160,80,30);
		limpiar.setBackground(Color.cyan);
		limpiar.setForeground(Color.BLACK);
		
		resultado.setText("Primo");
		resultado.setBounds(100,160,80,30);
		resultado.setBackground(Color.cyan);
		resultado.setForeground(Color.BLACK);
		
		volver.setText("Atras");
		volver.setBounds(190,160,80,30);
		volver.setBackground(Color.cyan);
		volver.setForeground(Color.BLACK);
		
		pr.add(titulo);
		pr.add(setnum_1);
		pr.add(getnum_1);
		pr.add(setres);
		pr.add(getres);	
		pr.add(resultado);
		pr.add(volver);
		pr.add(limpiar);
		
		limpiar.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent evt) {
    			getnum_1.setText("");
    			getres.setText("");
    		}
    	});
		
		resultado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				int num,m=2;
				boolean band = true;
				
				num = Integer.parseInt(getnum_1.getText());
				
				while ((band) && (m < num)) {
		            if ((num % m) == 0) {
		                band = false;
		            } else {
		                m = m + 1;
		            }
		        }

		        if (band) {
		            if (num <= 1) {
		                getres.setText("El Numero No Es Primo");
		            } else {
		                getres.setText("Tu numero es primo");
		            }
		        } else {
		        	getres.setText("El Numero No es primo");
		        }
			}
		});
		
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				interfaz.principal();
				pr.dispose();
			}
		});
		
		pr.setLocationRelativeTo(null);
		pr.setSize(300,250);
		pr.setVisible(true);
	}
}